# -*- coding: utf-8 -*-
{
    'name' : 'Table Tennis',
    'version' : '1.0',
    'summary': 'My game my rules',
    'sequence': -100,
    'description': """My game my rules""",
    'category': 'Productivity',
    'website': 'https://www.guptaji.com',
    'depends' : [],
    'data': [
        'security/ir.model.access.csv',
        'views/players.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
