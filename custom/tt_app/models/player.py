from odoo import api, fields, models

class TtPlayer(models.Model):
    _name = 'tt.player'
    _description = 'Record of tt players'

    name = fields.Char(string='Name', required=True)
    age = fields.Integer(string='Age')
    gender = fields.Selection([('male', 'Male'),('female', 'Female'),('others', 'Others'),], 
                                required=True, default='male')
    username = fields.Char(string='Username', required=True)
    email = fields.Char(string='Email', required=True)
    group = fields.Selection([('admin','Admin'), ('player','Player')])
    match_played = fields.Integer(string='Match Played')
    match_win = fields.Integer(string='Match Won')
    tournament=fields.Many2many('tt.tournament',string='tournament')

class Tournament(models.Model):
    _name='tt.tournament'
    _description='records of tournaments'

    #many to one for inverse
    name=fields.Char(string='Name of tournament', required=True)
    winner=fields.Many2one('tt.player',string='Winner')
    player_ids=fields.Many2many('tt.player', string='Players')
    typeofmatch=fields.Selection([('single','Singles'), ('doubles', 'Doubles')])


