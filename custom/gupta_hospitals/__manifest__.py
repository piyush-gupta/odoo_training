# -*- coding: utf-8 -*-
{
    'name' : 'Guptaji Hospital',
    'version' : '1.0',
    'summary': 'My hospital my rules',
    'sequence': -100,
    'description': """My hospital my rules""",
    'category': 'Productivity',
    'website': 'https://www.guptaji.com',
    'depends' : [
        'mail',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/patient.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
