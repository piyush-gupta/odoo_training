from odoo import api, fields, models

class HospitalPatient(models.Model):
    _name = "hospital.patient"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Hospital Patient"

    name = fields.Char(string='Name', required=True)
    age = fields.Integer(string='Age')
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('others', 'Others'),
    ], required=True, default='male')
    note = fields.Text(string='Description')
    status = fields.Selection([('draft','Draft'),('confirm','Confirmed'),
                                ('done','Done'),('cancelled','Cancelled')])
