from odoo import api, fields, models

class HospitalData(models.Model):
    _name = "hospital.customers"
    _description = 'Hospital Patient'

    name = fields.Many2one('patient.detail', string="patient")
    fees = fields.Float(string='Fees')
    diagnose = fields.One2many('patient.treatment', inverse_name='treated_for')

class Patient(models.Model):
    _name='patient.detail'
    _description="detail of patient"

    name = fields.Char()
    age = fields.Integer(string='Age')
    email = fields.Char(string='Email', required=True)
    gender = fields.Selection([('male', 'Male'),('female', 'Female'),('others', 'Others'),], 
                                required=True, default='male')


class Treatment(models.Model):
    _name='patient.treatment'
    _description='records of patients treatment'

    name = fields.Char(string="prescription")
    treated_for = fields.Many2one('hospital.customers')
    quantity = fields.Integer('Quantity')
    consumption_unit = fields.Selection([("bd","BD"),("od","OD")])

# class PateintData(models.Model):
#     _name = "patient.treatment.data"
#     _description = 'Hospital Patient'

#     customer_id = fields.Many2one('hospital.customers')
#     name = fields.Char(string="prescription")
#     quantity = fields.Integer('Quantity')
#     consumption_unit = fields.Selection([("bd","BD"),("od","OD")])

