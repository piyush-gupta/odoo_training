# -*- coding: utf-8 -*-
{
    'name' : 'New Hospital',
    'version' : '1.0',
    'summary': 'My hospital my rules',
    'sequence': -100,
    'description': """My hospital my rules""",
    'category': 'Productivity',
    'website': 'https://www.guptaji.com',
    'depends' : [],
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/customers.xml',
        'report/patient_card.xml',
        'report/report.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
